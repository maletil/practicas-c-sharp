﻿//  Prácticas C Sharp: Práctica 4.1 (Polinomio Taylor Coseno)
//  Miguel Ángel Sánchez Herreros <MiguelAngel.Sanchez16@alu.uclm.es>
//
//  Copyright (c) 2018 Miguel Ángel Sánchez Herreros
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_4._1
{
    class Program
    {
        static void Main(string[] args) {
            double resultado,angulo;
            Console.WriteLine("Introduce un ángulo en radianes");
            angulo = double.Parse(Console.ReadLine());
            Console.WriteLine();

        if(Fcoseno(angulo,8,out resultado))
            {
                Console.WriteLine("El resultado está por encima del eje X");
            } else
            {
                Console.WriteLine("El resultado está por debajo o en el eje X");
            }
            Console.WriteLine("El resultado es {0}",resultado);

        }

        public static bool Fcoseno(double angulo,int precision, out double resultado) {
            resultado = 0; 

            for(int n = 0; n < precision; n++) {
                resultado = resultado + Math.Pow(-1,n) * (Math.Pow(angulo,(2*n)) / Ffactorial(2*n));
               // Console.WriteLine("n={0}, el signo es {1}, resultado es {2}", n, Math.Pow(-1,n),resultado);
            }

            if (resultado > 0) { return true; } else { return false; }
        }
        public static int Ffactorial(int num){
            int factorial = 1;
            for (int i = 1; i <= num; i++) {
                factorial = factorial * i;
            }
            return factorial;
        }
    }
}
