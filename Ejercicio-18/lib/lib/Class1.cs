﻿using System;

namespace lib {
	public class Matrices{

		public static void Diagonal(char[,] matriz, char[] diagonalP,char[] diagonalS) {

			for (int i = 0; i < matriz.GetLength(0); i++) {
				diagonalP[i] = matriz[i,i];

				diagonalS[i] = matriz[i,3 - i];
			}
			
		}
		public static void SumaProducto(char[,] matriz, out int s1, out int p1,out int s2,out int p2) {
			s1 = 0; p1 = 1; s2 = 0;p2 = 1;
			char[] diagonalP = new char[4];
			char[] diagonalS = new char[4];
			Diagonal(matriz,diagonalP,diagonalS);

			for(int i = 0; i< diagonalP.Length;i++) {
				s1 += diagonalP[i];
				p1 = p1 * diagonalP[i];
			}
			for (int i = 0; i < diagonalS.Length; i++) {
				s2 +=diagonalS[i];
				p2 = p2 * diagonalS[i];
			}
		}
		public static void MayorVectores(int[] vector1, int[] vector2, out int n1, out int n2) {
			n1 = Mayor(vector1);
			n2 = Mayor(vector2);
		}
		private static int Mayor(int[] vector) {
			int n = 0;
			for (int i = 0; i < vector.Length - 1; i++) { //Vector
				if (vector[i] > n) {
					n = vector[i];
				}
			}
			return n;
		}

		/*
		 private static int[] MayorVector(int[] vector) {
			int[] vector_final = new int[vector.Length];
			int pos = 0;
			for (int j = 0; j < vector.Length-1; j++) {
				vector_final[j] = MayorPos(vector,ref pos);
				vector[pos] = 0;
			}		
			return vector_final;
		}
		
		private static int MayorPos(int[] vector, ref int pos) {
			int n = 0; 
			pos = 0;
			for (int i = 0; i < vector.Length-1; i++) { //Vector
				if (vector[i] > n) {
					n = vector[i];
					pos = i;
				}
			}
			return n;
		}
		*/
	}
}
