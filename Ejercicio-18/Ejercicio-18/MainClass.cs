﻿//  Prácticas C Sharp: Ejercicio 29, REL.PROB 4 (Manipulación de matrices)
//  Miguel Ángel Sánchez Herreros <MiguelAngel.Sanchez16@alu.uclm.es>
//
//  Copyright (c) 2018 Miguel Ángel Sánchez Herreros
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
using System;
using lib;
namespace Ejercicio18 {
	public class MainClass {
		public static void Main() {
			char[,] matriz = {  { 'a','g','u','a' },
								{ 'm','o','z','o' },
								{ 'p','a','t','o' },
								{ 'i','d','e','a' }
							 };
			char[,] matrizN = { { '1','2','3','4' },
								{ '8','7','6','5' },
								{ '9','0','1','2' },
								{ '5','6','7','9' }
							 };
			int[] vector1 = { 0,2,5,5,123,4,131,24,52 };
			int[] vector2 = { 1,4,523,636,31,79,0,123,-12 };
			char[] diagonalP = new char[4];
			char[] diagonalS = new char[4];
			int s1, s2, p1, p2;
			int n1 = 0, n2 = 0;
			bool continuar = true;

			while (continuar) {
				Console.WriteLine("¿Qué quieres calcular?");
				Console.WriteLine("0. Salir del programa");
				Console.WriteLine("1. Diagonales de la matriz");
				Console.WriteLine("2. Suma y producto de las diagonales de una matriz");
				Console.WriteLine("3. Mayores elementos de dos vectores");
				int opcion = int.Parse(Console.ReadLine());

				while (opcion < 0 || opcion > 3) {
					Console.WriteLine("Opción incorrecta. Introduce una de las tres opciones.");
					opcion = int.Parse(Console.ReadLine());
				}
				switch (opcion) {
					case 0:
						continuar = false;
						break;
					case 1:

						Matrices.Diagonal(matriz,diagonalP,diagonalS);

						for (int i = 0; i < 4; i++) {
							Console.Write("{0} ",diagonalP[i]);
						}
						Console.WriteLine();
						for (int i = 0; i < 4; i++) {
							Console.Write("{0} ",diagonalS[i]);
						}
						Console.WriteLine();
						break;
					case 2:

						Matrices.SumaProducto(matriz,out s1,out p1,out s2,out p2);
						Console.WriteLine("Sumas: {0},{1}, productos: {2},{3}",s1,s2,p1,p2);
						break;
					case 3:
						Matrices.MayorVectores(vector1,vector2,out n1,out n2);
						Console.WriteLine("Los números mayores de cada vector son: {0} y {1}",n1,n2);
						break;
				}


				Console.WriteLine();
			}


			/*int pos = 0;
			Console.WriteLine("{0} en {1}",Matrices.Mayor(numeros, ref pos),pos);
			int[] ordenados = Matrices.MayorVector(numeros);
			for (int i = 0; i < ordenados.Length; i++) {
				Console.Write("{0} ",ordenados[i]);
			}
			*/

		}
	}
}
