﻿using System;

namespace lib {
	public class Gris {
		public static int[] Devolver(int [,] matriz){
			int longitud_fila = matriz.GetLength(0);
			int longitud_columna = matriz.GetLength(1);

			if (longitud_fila % 2 != 0){
				int[] vector = new int[(longitud_columna - 1)*(longitud_fila - 1)]; //Longitud del vector
				int k = 0; //Posición en el vector

				int columna_central = (longitud_fila - 1) / 2; //Para que empiece a contar desde 0

				for (int i = 1; i < longitud_columna ; i ++){ //Filas
					for (int j = 0; j < longitud_fila;j++){ //Columnas
						if (j == columna_central){
							continue;
						}
						vector[k] = matriz[i,j];
						k++;
					}
				}
				return vector;
			}
			return null;

		}
	}
}
