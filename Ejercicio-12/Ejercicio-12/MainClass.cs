﻿//  Prácticas C Sharp: Ejercicio 12, REL.PROB 3 (con lib).
//  Miguel Ángel Sánchez Herreros <MiguelAngel.Sanchez16@alu.uclm.es>
//
//  Copyright (c) 2018 Miguel Ángel Sánchez Herreros
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
using System;
using lib;
namespace Ejercicio12 {
	public class MainClass {
		public static void Main() {
			Console.WriteLine("Introduce la x de un punto:");
			float x = float.Parse(Console.ReadLine());
			Console.WriteLine("Introduce la y:");
			float y = float.Parse(Console.ReadLine());
			//Bucle
			do {
				Console.WriteLine("Elije una opción:");
				Console.WriteLine("0. Salir del programa");
				Console.WriteLine("1. ¿El punto está en una circunferencia?");
				Console.WriteLine("2. ¿En qué cuadrante está el punto?");
				Console.WriteLine("3. Conversión a coordenadas polares.");

				float opcion = float.Parse(Console.ReadLine());
				Console.WriteLine();

				switch (opcion) {
					case 1:
						Console.WriteLine("Introduce el radio de la circunferencia:");
						float r = float.Parse(Console.ReadLine());
						Console.WriteLine("Introduce la x del centro de la circunferencia:");
						float x2 = float.Parse(Console.ReadLine());
						Console.WriteLine("Introduce la y del centro de la circunferencia:");
						float y2 = float.Parse(Console.ReadLine());

						if (Puntos.FenCircunferencia(x,y,r,x2,y2)) {
							Console.WriteLine("El punto está dentro de la circunferencia");
						} else {
							Console.WriteLine("El punto no está dentro de la circunferencia.");
						}
						break;
					case 2:

						int cuadrante = Puntos.Fcuadrante(x,y);
						switch (cuadrante) {
							case 0:
								Console.WriteLine("El punto está en el origen de coordenadas");
								break;
							case 5:
								Console.WriteLine("El punto está en un eje");
								break;
							default:
								Console.WriteLine("El punto está en el cuadrante {0}",cuadrante);
								break;
						}
						break;
					case 3:
						float angulo = 0, radio = 0;
						Puntos.Fpolares(x,y,ref angulo,ref radio);
						Console.WriteLine("El ángulo es {0} y el radio {1}",angulo,radio);
						break;
					case 0:
						Environment.Exit(0); //Acabar.
						break;
				}
			} while (true);

		}

	}
}
