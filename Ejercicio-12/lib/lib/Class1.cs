﻿using System;

namespace lib {
	public class Puntos {
		// Cálculo

		public static Boolean FenCircunferencia(float x,float y,float r,float x2,float y2) {
			if (Math.Round(Math.Pow((x - x2),2) + Math.Pow((y - y2),2)) < Math.Round(Math.Pow(r,2))) {
				return true;
			} else {
				return false;
			}
		}
		public static int Fcuadrante(float x,float y) {
			if (x == 0 && y == 0) {
				return 0;
			} else if (x == 0 || y == 0) {
				return 5;
			} else if (x < 0) {
				if (y > 0) {
					return 2;
				}
				return 3;
			} else {
				if (y > 0) {
					return 1;
				}
				return 4;
			}
		}
		public static void Fpolares(float x,float y,ref float angulo,ref float radio) {
			angulo = (float)Math.Atan((y / x));
			radio = (float)Math.Sqrt(Math.Pow(x,2) + Math.Pow(y,2));
		}
	}
}
