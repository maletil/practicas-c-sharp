﻿using System;

namespace lib {
	public static class Triangulo {
		public static void DevolverLongitudes(out float lado1,out float lado2,out float lado3) {
			lado1 = float.Parse(Console.ReadLine());
			lado2 = float.Parse(Console.ReadLine());
			lado3 = float.Parse(Console.ReadLine());
		}
		public static void DevolverAngulos(out float angulo1,out float angulo2,out float angulo3) {
			angulo1 = float.Parse(Console.ReadLine());
			angulo2 = float.Parse(Console.ReadLine());
			angulo3 = float.Parse(Console.ReadLine());
		}
		/// <summary>
		/// 
		/// </summary>
		/// <returns>The equilatero.</returns>
		/// <param name="lado1">Lado del triángulo original</param>
		/// <param name="lado2">Base del triángulo</param>
		/// <param name="lado3">Lado opuesto del triángulo.</param>
		/// <param name="lado4">Altura de los triángulos resultantes</param>
		/// <param name="lado5">Base de los triángulos resultantes</param>
		/// <param name="lado6">Hipotenusa de los triángulos resultantes</param>
		public static int DescomponerEquilatero(float lado1,float lado2,float lado3,ref float lado4, ref float lado5, ref float lado6) {
			if (lado1 == lado2 && lado2 == lado3){
				lado4 = (float)Math.Sqrt(Math.Pow(lado1,2) - Math.Pow((lado2 / 2),2));
				lado5 = lado2 / 2;
				lado6 = lado1;
				return 1;
			}
			return 0;
		}
		public static Boolean ComprobarObtusangulo(float angulo1,float angulo2,float angulo3) {
			if (angulo1 > 90 || angulo2 > 90 || angulo3 > 90) {
				return true;
			}
			return false;
		}
	}
}
