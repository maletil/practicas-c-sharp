﻿//  Prácticas C Sharp: Ejercicio 17, REL.PROB 3 (Triángulos)
//  Miguel Ángel Sánchez Herreros <MiguelAngel.Sanchez16@alu.uclm.es>
//
//  Copyright (c) 2018 Miguel Ángel Sánchez Herreros
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
using System;
using System.Threading;
using lib;

namespace Ejercicio11 {
	public class MainClass {
		public static void Main() {
			do {
				float ladoA,ladoB,ladoC,ladoD,ladoE,ladoF,ladoH = 0, ladoI = 0, ladoJ = 0;

				Console.WriteLine("Dame las longitudes de los lados del primer triángulo:");
				Triangulo.DevolverLongitudes(out ladoA, out ladoB, out ladoC);
				Console.WriteLine("Dame las longitudes de los lados del segundo triángulo:");
				Triangulo.DevolverLongitudes(out ladoD,out ladoE,out ladoF);

				float anguloA, anguloB, anguloC, anguloD, anguloE, anguloF;
				Console.WriteLine("Dame los ángulos del primer triángulo:");
				Triangulo.DevolverAngulos(out anguloA,out anguloB,out anguloC);
				Console.WriteLine("Dame los ángulos del segundo triángulo:");
				Triangulo.DevolverAngulos(out anguloD,out anguloE,out anguloF);

				Console.WriteLine();
				Thread.Sleep(1000);

				int descompuesto = Triangulo.DescomponerEquilatero(ladoA,ladoB,ladoC,ref ladoH, ref ladoI, ref ladoJ);
				if (descompuesto == 1){
					Console.WriteLine("Los lados de los dos triángulos serán:");
					Console.WriteLine("\tAltura: {0}\n\tBase: {1}\n\tHipotenusa: {2}",ladoH,ladoI,ladoJ);
				} else{
					Console.WriteLine("No se puede descomponer el triángulo 1 en triángulos rectángulos iguales.");
				}

				Thread.Sleep(1000);

				Console.Write("\nEl primer triángulo ");
				if(Triangulo.ComprobarObtusangulo(anguloA,anguloB,anguloC)){
					Console.Write("sí");
				} else {
					Console.Write("no");
				}
				Console.WriteLine(" es obtusángulo");
				Console.Write("El segundo triángulo ");
				if (Triangulo.ComprobarObtusangulo(anguloD,anguloE,anguloF)){
					Console.Write("sí");
				} else {
					Console.Write("no");
				}
				Console.WriteLine(" es obtusángulo");
				
				Console.WriteLine();
				Thread.Sleep(500);
				Console.WriteLine("¿Quieres seguir usando la calculadora? (s/n)");
				char r = char.Parse(Console.ReadLine());
				if (char.ToLower(r) != 's') {
					break;
				}
			} while (true);

		}
	}
}
