﻿//  Prácticas C Sharp: Práctica 5 (Matrices)
//  Miguel Ángel Sánchez Herreros <MiguelAngel.Sanchez16@alu.uclm.es>
//
//  Copyright (c) 2018 Miguel Ángel Sánchez Herreros
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_5
{
    class Program
    {
        static void Main(string[] args)
        {
            char[,] M1 = new char[,] { {'b','c','d','e'},
                                       {'f','g','h','i'},
                                       {'j','k','l','m'},
                                       {'n','o','p','q'}
                                     };

            char[,] M2 = new char[M1.GetLength(0),M1.GetLength(1)];
            int ceros = RellenaCeros(M1, M2);
            imprimirMatriz(M2);
            Console.WriteLine("Se han puesto {0} ceros", ceros);

            Console.WriteLine();
            char[] vector_1 = new char[M1.GetLength(1)];
            char[] vector_2 = new char[M1.GetLength(0)];

            Dibu_Z(M1,'*', vector_1, vector_2); // DIAGONAL[I] = MATRIZ[I,3-1]
            imprimirMatriz(M1);
            Console.WriteLine("Vector1:");
            imprimirVector(vector_1);
            Console.WriteLine("Vector2:");
            imprimirVector(vector_2);
        }

        public static int RellenaCeros(char[,] m1, char[,] m2)
        {
            int x_mitad = m1.GetLength(1) / 2;
            int y_mitad = m1.GetLength(0) / 2;
            int n = 0;

            for (int i = 0; i < m1.GetLength(0);i++)  { //Filas
                for (int j = 0; j < m1.GetLength(1); j++) { //Elementos/Columnas
                    if (j >= x_mitad && i < y_mitad) {
                        m2[i, j] = '0';
                        n++;
                    } else {
                        m2[i, j] = m1[i, j];
                    }
                }

            }
            return n;
        }

        public static void Dibu_Z(char[,] matriz, char caracter, char[] vector_1, char[] vector_2) {

            for (int i = 0; i < matriz.GetLength(1); i++) //Primer vector
            {
                vector_1[i] = matriz[0, i];
            }
            //Diagonal secundaria y segundo vector
            for (int z = 0; z < vector_2.Length; z++) {
                vector_2[z] = matriz[z, vector_2.Length -1- z];
                matriz[z, vector_2.Length - 1 - z] = caracter;
            }
            for (int i = 0; i < matriz.GetLength(1); i++) //Sustituir primera fila
            {
                matriz[0, i] = caracter;
            }
            for (int i = 0; i < matriz.GetLength(1); i++) //sust últ
            {
                matriz[matriz.GetLength(0)-1, i] = caracter;
            }
        }

        //Este método imprime una matriz de caracteres de tamaño cualquiera
        static void imprimirMatriz(char[,] M)
        {
            for (int i = 0; i < M.GetLength(0); i++)
            {
                for (int j = 0; j < M.GetLength(1); j++)
                {
                    Console.Write("{0}    ", M[i, j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
        //Este método imprime un vector de caracteres de tamaño cualquiera 
        static void imprimirVector(char[] V)
        {
            //Console.WriteLine("imprimo vector");
            for (int i = 0; i < V.Length; i++)
            {
                Console.Write("{0} ", V[i]);
            }
            Console.WriteLine();
        }

    }
}
