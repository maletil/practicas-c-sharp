﻿using System;

namespace lib {
	public class Notas {

		static int[] Redondear(float[] vector1){
			int[] vector2 = new int[vector1.Length];
			float parteDecimal = 0;

			for (int i = 0; i < vector1.Length; i++){
				parteDecimal = vector1[i] - (int)vector1[i];
				if (parteDecimal >= 0.5){
					vector2[i] = (int)vector1[i]+1;
				} else{
					vector2[i] = (int)vector1[i];
				}
			}
			return vector2;
		}

		public static int[] Frecuencia(float[] notas){
			int[] redondeado = Redondear(notas);

			int[] frecuencia = new int[11];

			for (int i = 0; i <= 10; i++) {
				for (int e = 0; e < redondeado.Length; e++) {
					if (redondeado[e] == i){
						frecuencia[i]++;
					}
				}
			}
			return frecuencia;
		}
	}
}
