﻿//  Prácticas C Sharp: Ejercicio 24, REL.PROB 4 (Frecuencia de notas)
//  Miguel Ángel Sánchez Herreros <MiguelAngel.Sanchez16@alu.uclm.es>
//
//  Copyright (c) 2018 Miguel Ángel Sánchez Herreros
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
using System;
using lib;
namespace Ejercicio14 {
	public class MainClass {
		public static void Main() {
			Console.WriteLine("Introduce el número de alumnos");

			float[] notas = new float[int.Parse(Console.ReadLine())];
			for (int i = 0; i < notas.Length; i++) {
				Console.WriteLine("Introduce la nota {0}",i+1);
				notas[i] = float.Parse(Console.ReadLine());
			}
			int[] frecuencia = Notas.Frecuencia(notas);

			Console.WriteLine();
			for (int e = 0; e < frecuencia.Length; e++){
				Console.WriteLine("Números de {0}: {1}",e,frecuencia[e]);
			}


		}
	}
}
