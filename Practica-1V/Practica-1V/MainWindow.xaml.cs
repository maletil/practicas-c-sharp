﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Practica_1V
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        float V_primernumero = 0, V_segundonumero = 0;
        double V_producto, V_suma;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            V_primernumero = float.Parse(primernumero.Text);
            V_segundonumero = float.Parse(segundonumero.Text);

            V_producto = V_primernumero * V_segundonumero;
            V_suma = V_primernumero + V_segundonumero;

            producto.Text = V_producto.ToString();
            suma.Text = V_suma.ToString();
        }
    }
}
