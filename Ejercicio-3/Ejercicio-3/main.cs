﻿//  Prácticas C Sharp: Ejercicio 35, REL.PROB 2 (Cálculo de ecuaciones de segundo grado)
//  Miguel Ángel Sánchez Herreros <MiguelAngel.Sanchez16@alu.uclm.es>
//
//  Copyright (c) 2018 Miguel Ángel Sánchez Herreros
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
namespace main {
	class MainClass {
		public static void Main (string [] args)
		{
			//float grado1 = -35, grado2 = -2, grado3 = 1;
			//float grado1 = 5, grado2 = -2, grado3 = 1;
			float grado1, grado2, grado3;
			Console.WriteLine ("Dame números");
			Console.WriteLine ("Número del grado 0");
			grado1 = float.Parse (Console.ReadLine());
			Console.WriteLine ("Número del grado 1");
			grado2 = float.Parse (Console.ReadLine ());
			Console.WriteLine ("Número del grado 2");
			grado3 = float.Parse (Console.ReadLine ());
			double resultado1, resultado2;


			// Cálculo
			float dentrodelaraiz;
			dentrodelaraiz = (float)(Math.Pow(grado2,2)) - (float)(4 * grado1 * grado3);
			//Console.WriteLine (dentrodelaraiz);
			if (dentrodelaraiz >= 0) {
				Console.WriteLine ("Resultados: ");
				//todo ok
				resultado1 = (-grado2 + Math.Sqrt (dentrodelaraiz)) / (2*grado3);

				resultado2 = (-grado2 - Math.Sqrt (dentrodelaraiz)) / (2 * grado3);
				Console.WriteLine ("{0} y {1}",resultado1,resultado2);
			} else { //Números imaginarios
				float primeraparte, segundaparte;

				primeraparte = -grado2 / (2 * grado3);
				segundaparte = (float)Math.Sqrt (Math.Abs(dentrodelaraiz)) / (2 * grado3);
				Console.WriteLine ("{0} ± {1}i",primeraparte,segundaparte);
			}
		}
	}
}