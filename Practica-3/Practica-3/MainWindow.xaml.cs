﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Practica_3
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int bin = int.Parse(entrada.Text);
            if (bin < 0)
            {
                MessageBox.Show("Introduce un número positivo", "Error");
            }
            else
            {
                resultado.Text = Convertir(bin).ToString();
            }
        }
        public static int Convertir(int bin)
        {
            int sumatorio = 0, longitud,dig;
            
            for (longitud = 0; bin != 0; longitud++)
            {
                dig = bin % 10;
                sumatorio += dig*(int)(Math.Pow(2,longitud));

                bin = bin / 10; // Condición de paro
            }
            return sumatorio;
        }
    }
}
