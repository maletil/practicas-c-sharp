﻿using System;

namespace lib {
	public class Columnas {
		public static int Suma(int[,] matriz, int columna) {
			int suma = 0;
			for (int i = 0; i < matriz.GetLength(0);i++){
				suma += matriz[i,columna];
			}
			return suma;
		}

		public static void Sustituye(int[,] matriz, out int mayor, out int menor){
			int[] sumas = new int[matriz.GetLength(1)];

			for (int columna = 0; columna < sumas.Length;columna++){
				sumas[columna] = Suma(matriz,columna);
				if(sumas[columna] >=25){
					for (int i = 0; i < matriz.GetLength(0);i++){
						matriz[i,columna] = sumas[columna];
					}
				}
			}

			/*for (int lah = 0; lah < matriz.GetLength(1);lah++){
				for (int lab = 0; lab < matriz.GetLength(0);lab++){
					Console.Write(matriz[lah,lab].ToString());
				}
				Console.WriteLine();
			}*/

			mayor = sumas[0];
			menor=sumas[0];
			for (int e = 0; e < sumas.Length;e++){
				if(sumas[e] > mayor){
					mayor = sumas[e];
				}
				if(sumas[e]<menor){
					menor = sumas[e];
				}
			}
		}

		public static void CrearMatriz(int[] vector, int[,] matriz) {

			for (int c = 0; c < 6;c++) { //número de columna
				for (int f = 0; f < 6;f++){
					//Metemos en columnas
					int posVector = c * 6 + f;
					//Console.WriteLine("C {0}, F {1},  POSVECTOR{2}",c,f,posVector);
					matriz[f,c] = vector[posVector];
					//Console.WriteLine(vector[posVector].ToString());
				}

			}
		}
	}
}
