﻿//  Prácticas C Sharp: Ejercicio 32, REL.PROB 4 (Operaciones con matrices)
//  Miguel Ángel Sánchez Herreros <MiguelAngel.Sanchez16@alu.uclm.es>
//
//  Copyright (c) 2018 Miguel Ángel Sánchez Herreros
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
using System;
using lib;
namespace Ejercicio15 {
	public class MainClass {
		public static void Main() {
			int mayor, menor;
			int[,] matriz = {{1,2,3,4,5},{1,2,3,4,5},{1,2,3,4,5},{1,2,3,4,5},{1,2,3,4,5}};

			Console.WriteLine("Suma de la columna 0: {0}",Columnas.Suma(matriz,0));

			Columnas.Sustituye(matriz, out mayor, out menor);

			for (int i = 0; i < 5; i++) {
				for (int j = 0; j < 5; j++) {
					Console.Write("{0:D2} ",matriz[i,j]);
				}
				Console.WriteLine();
			}
			Console.WriteLine("Mayor: {0}, menor: {1}",mayor,menor);

			int[] vector = { 01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36 };
			int[,] matriz2 = new int[6,6];
			Columnas.CrearMatriz(vector, matriz2);

			for (int i = 0; i < 6; i++) {
				for (int j = 0; j < 6; j++) {
					Console.Write("{0:D2} ",matriz2[i,j]);
				}
				Console.WriteLine();
			}
		}
	}
}
