﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Practica_2._1
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            // Generar números perfectos.
            int agenerar = int.Parse(numero.Text);
            int minimo = int.Parse(min.Text);
            resultado.Text = "";

            if (agenerar > 6 || agenerar < 2) {
                MessageBox.Show("Por favor introduce un número entero comprendido 1 y 5.", "Número inválido");
            } else {
                //contador es el número de números correctos
                int contador = 0;
                int probar = minimo+1;
                while (contador < agenerar) {
                    Console.WriteLine("VUELTA");
                    int numeroValido = 0;

                        for (int i = 1; i < probar; i++) //TODOS LOS NUMEROS HASTA probar
                        {
                            if (probar % i == 0)
                            {
                                numeroValido = numeroValido + i;
                            }
                        }
                        if (numeroValido == probar)
                        { 
                            resultado.Text += numeroValido + ", ";
                            contador++;
                        }
                        probar++;
                }
            }

        }
    }
}
