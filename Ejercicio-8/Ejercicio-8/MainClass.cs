﻿//  Prácticas C Sharp: Ejercicio 23, REL.PROB 2 (Polinomio de Taylor del cos)
//  Miguel Ángel Sánchez Herreros <MiguelAngel.Sanchez16@alu.uclm.es>
//
//  Copyright (c) 2018 Miguel Ángel Sánchez Herreros
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
using System;
namespace Ejercicio8 {
	public class MainClass {
		static public void Main() {

			Console.WriteLine("Dime el numero de términos de la serie (n>=1): ");
				int n = int.Parse(Console.ReadLine());
			Console.WriteLine("Dime el valor de x: ");
				double x = double.Parse(Console.ReadLine());

			Console.WriteLine("\nEl resultado es {0}\n",Fcoseno(n,x));
		}
		// Cálculo

		public static double Fcoseno(int n, double x){
			double sumatorio = 0.0;//para almacenar el resultado
			int numeros_pares = 0;//para almacenar los números pares
			int terminos = 0;//para controlar los términos de la serie
			int positivo = 1;//para controlar si el termino es positivo o negativo
			double factorial;

			for (; ; )
				{
				//Este bucle calcula el factorial del número par que toca en la vuelta
				factorial = FcalculoFactorial(numeros_pares);

				//Calculamos el resto de la expresión
				sumatorio = sumatorio + (positivo * FcalculoPotencia(x,numeros_pares) / factorial);
				terminos++;
				numeros_pares = numeros_pares + 2;
				positivo = -positivo;
				if (terminos == n) {
					break;
				}
			}
			return sumatorio;
		}

		public static double FcalculoPotencia(double x, int num){
			double resultado = 1;
			for (int i = 0; i < num;i++){
				resultado = resultado * x;
			}
			return resultado;
		}

		public static double FcalculoFactorial(int num){
			double factorial = 1;
			for (int j = 1; j <= num; j++) {
				factorial = factorial * j;
			}
			return factorial;
		}
	}
}
