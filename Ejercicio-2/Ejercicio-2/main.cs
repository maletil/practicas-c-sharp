﻿//  Prácticas C Sharp: Ejercicio 32, REL.PROB 2 (Sucesión Fibonacci)
//  Miguel Ángel Sánchez Herreros <MiguelAngel.Sanchez16@alu.uclm.es>
//
//  Copyright (c) 2018 Miguel Ángel Sánchez Herreros
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
namespace Ejercicio2 {
	public class main {
		public static void Main(string[] args) {
			//Definimos cuántos números mostrar (3+)
			int cuantosnum = 20;

			int primero = 0, segundo = 1;
			int resultado = 0;
			Console.WriteLine(primero);
			Console.WriteLine(segundo);
			for (int contador = 0; contador < cuantosnum - 2; contador++) {
				resultado = primero + segundo;
				primero = segundo;
				segundo = resultado;

				Console.WriteLine(resultado);
			}
		}
	}
}
