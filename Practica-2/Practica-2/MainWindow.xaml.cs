﻿//  Prácticas C Sharp: Práctica 2, parte 1
//  Miguel Ángel Sánchez Herreros <MiguelAngel.Sanchez16@alu.uclm.es>
//
//  Copyright (c) 2018 Miguel Ángel Sánchez Herreros
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Practica_2 {
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e) {
            // Comprobar números perfectos.
            int Vnumero = int.Parse(numero.Text);

            if (Vnumero < 0) {
                MessageBox.Show("Por favor introduce un número natural positivo.", "Número inválido");
            } else {
                int numeroTotal = 0;
                for (int i = 1; i < Vnumero; i++) {
                    int resto = 0;
                    resto = Vnumero % i;
                    if (resto == 0) {
                        numeroTotal = numeroTotal + i;
                    }
                }

                if (numeroTotal == Vnumero) {
                    resultado.Text = "Es.";
                } else {
                    resultado.Text = "No es.";
                }
            }

            }
        }
    }

