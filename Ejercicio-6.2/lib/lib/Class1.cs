﻿using System;

namespace lib {
	public class ResolverEcuacion {

		/// <summary>
		/// Calcula el discriminante.
		/// </summary>
		/// <param name="grado1">Coeficiente del grado 2</param>
		/// <param name="grado2">Coeficiente del grado 1</param>
		/// <param name="grado3">Término independiente</param>
		/// <returns> Discriminante</returns>
		static float Fdiscriminante(int grado1,int grado2,int grado3) {
			return (float)(Math.Pow(grado2,2)) - (float)(4f * grado1 * grado3);
		}

		/// <summary>
		/// Calcula la ecuación de segundo grado.
		/// </summary>
		/// <param name="grado1">Coeficiente del grado 2</param>
		/// <param name="grado2">Coeficiente del grado 1</param>
		/// <param name="grado3">Término independiente</param>
		/// <param name="resp1">Parte real del número complejo o primera raíz simple.</param>
		/// <param name="resp2">Parte imaginaria del número complejo o segunda raíz simple.</param>
		/// <returns>Devuelve True si la solución es un número complejo y False en caso contrario.</returns>
		public static Boolean Resolver(int grado1,int grado2,int grado3,out double resp1,out double resp2) {
			float discriminante = Fdiscriminante(grado1,grado2,grado3);
			if (discriminante >= 0) {
				resp1 = (-grado2 + Math.Sqrt(discriminante)) / (2 * grado1);
				resp2 = (-grado2 - Math.Sqrt(discriminante)) / (2 * grado1);
				return false;
			} else { //Números imaginarios

				resp1 = (double)-grado2 / (2 * grado1);
				resp2 = (double)Math.Sqrt(-discriminante) / (2 * grado1);
				return true;
			}
		}

		/// <summary>
		/// Pide un coeficiente en consola y devuelve su valor si no es cero.
		/// </summary>
		/// <param name="num"> Letra del coeficiente.</param>
		/// <returns>Valor introducido.</returns>
		public static int FpedirNumeros(char num) {
			int respuesta;
			Console.WriteLine("Introduce el número {0}",num);
			respuesta = int.Parse(Console.ReadLine());
			while (respuesta == 0) {
				Console.WriteLine("El número no puede ser 0, introduce el número {0}",num);
				respuesta = int.Parse(Console.ReadLine());
			}
			return respuesta;
		}

	}
}
