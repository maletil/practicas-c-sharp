﻿//  Prácticas C Sharp: Ejercicio 35, REL.PROB 2 (Con librería)
//  Miguel Ángel Sánchez Herreros <MiguelAngel.Sanchez16@alu.uclm.es>
//
//  Copyright (c) 2018 Miguel Ángel Sánchez Herreros
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
using System;
using lib;

namespace Ejercicio6 {
	public class MainClass {
		static public void Main() {
			//Iniciar variables
			int grado1, grado2, grado3;
			double respuesta1, respuesta2;

			//Pedir datos de entrada
			Console.WriteLine("Dame números en el formato ax²+bx+c");
			grado1 = ResolverEcuacion.FpedirNumeros('a');
			grado2 = ResolverEcuacion.FpedirNumeros('b');
			grado3 = ResolverEcuacion.FpedirNumeros('c');

			Console.WriteLine("");

			if (ResolverEcuacion.Resolver(grado1,grado2,grado3,out respuesta1,out respuesta2)) {
				Console.WriteLine("Los resultados son: {0} ± {1} i",respuesta1,respuesta2);
			} else {
				Console.WriteLine("Los resultados son: {0} y {1}",respuesta1,respuesta2);
			}
			if (ResolverEcuacion.Resolver(5,-4,1,out respuesta1,out respuesta2)) {
				Console.WriteLine("Los resultados son: {0} ± {1} i",respuesta1,respuesta2);
			} else {
				Console.WriteLine("Los resultados son: {0} y {1}",respuesta1,respuesta2);
			}
		}
	}
}