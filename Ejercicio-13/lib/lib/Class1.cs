﻿using System;

namespace lib {
	public class Vocales {
		public static int Contar(String cadena) {
			int nVocales = 0;

			for (int i = 0; i < cadena.Length; i++) {
				if (cadena[i] == ' '){
					break;
				}
				char min = Char.ToLower(cadena[i]);
				if (min == 'a' || min == 'e' || min == 'i' || min == 'o' || min == 'u') {
					nVocales++;

				//	Console.WriteLine("{0}",cadena[i]);
				}
			}

			return nVocales;
		}
	}

}