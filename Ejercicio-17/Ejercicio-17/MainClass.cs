﻿//  Prácticas C Sharp: Ejercicio 25, REL.PROB 4 (Cifrado César)
//  Miguel Ángel Sánchez Herreros <MiguelAngel.Sanchez16@alu.uclm.es>
//
//  Copyright (c) 2018 Miguel Ángel Sánchez Herreros
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
using System;
using lib;
namespace Ejercicio17 {
	public class MainClass {
		public static void Main() {

			Console.WriteLine("Introduce una frase a codificar:");
			string cadena = Console.ReadLine();
			Console.WriteLine("Indica el desplazamiento (positivo)");
			int desplazamiento = int.Parse(Console.ReadLine());


			string codificado = CSharp.Codificar(cadena,desplazamiento);
			Console.WriteLine(codificado);

			string deco = CSharp.Descodificar(codificado,desplazamiento);
			Console.WriteLine(deco);
		}
	}
}
