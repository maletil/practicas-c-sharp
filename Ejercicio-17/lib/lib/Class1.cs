﻿using System;

namespace lib {
	public class CSharp {
		public static string Codificar(string cadena, int desp){
			string abecedario = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
			char[] cadena_cifrada = new char[cadena.Length];

			for (int i = 0; i < cadena.Length; i++) { //Recorrer cadena
				bool encontrado = false; //Para cada nueva comprobación,
				for (int j = 0; j < abecedario.Length; j++) {
					if (abecedario[j] == cadena[i]) { //Encuentra el caracter
						encontrado = true;

						if ((j + desp) >= abecedario.Length) {//Se pasa del vector
							cadena_cifrada[i] = abecedario[(j + desp)- abecedario.Length];
						} else {
							cadena_cifrada[i] = abecedario[j + desp]; //Inserta el caracter desplazado en la nueva cadena
						}
					}
				}

				if (!encontrado) {
					cadena_cifrada[i] = cadena[i]; //Si no lo encuentra, pone la misma.
				}
			}
			return new string(cadena_cifrada);
		}
		public static string Descodificar(string cadena,int desp) {
			string abecedario = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
			char[] cadena_cifrada = new char[cadena.Length];

			for (int i = 0; i < cadena.Length; i++) { //Recorrer cadena
				bool encontrado = false; //Para cada nueva comprobación,
				for (int j = 0; j < abecedario.Length; j++) { //Posición en abecedario
					if (abecedario[j] == cadena[i]) { //Encuentra el caracter
						encontrado = true;

						if ((j - desp) < 0) { //Se pasa del vector
							cadena_cifrada[i] = abecedario[(j - desp) + abecedario.Length];
						} else {
							cadena_cifrada[i] = abecedario[j - desp]; //Inserta el caracter desplazado en la nueva cadena
						}
					}
				}

				if (!encontrado) {
					cadena_cifrada[i] = cadena[i]; //Si no lo encuentra, pone la misma.
				}
			}
			return new string(cadena_cifrada);
		}
	}
}
