﻿//  Prácticas C Sharp: Ejercicio 12, REL.PROB 3 (Coordenadas de un punto)
//  Miguel Ángel Sánchez Herreros <MiguelAngel.Sanchez16@alu.uclm.es>
//
//  Copyright (c) 2018 Miguel Ángel Sánchez Herreros
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
using System;
namespace Ejercicio9 {
	public class MainClass {
		public static void Main() {
			Console.WriteLine("{0}", (int)Math.Round((264.9998)));

			Console.WriteLine("BUENOS DÉAS. Dame la x");
			float x = float.Parse(Console.ReadLine());
			Console.WriteLine("Muy bien. Ahora me das la y, eh?");
			float y = float.Parse(Console.ReadLine());

			Console.WriteLine("Aquehas venido");
			Console.WriteLine("0. A nada");
			Console.WriteLine("1. ¿El punto está en una circunferencia?");
			Console.WriteLine("2. ¿En qué cuadrante está le punto?");
			Console.WriteLine("3. Conversión a coordenadas polaricas.");

			float opcion = float.Parse(Console.ReadLine());

			switch(opcion){
				case 0:
					Console.WriteLine("Venga, hasta luego. Gracias por colaborarr con la science.");
					break;
				case 1:
					Console.WriteLine("Muy bien, manos a la obra. Dame el radio:");
					float r = float.Parse(Console.ReadLine());
					Console.WriteLine("Muy bien Dame la x del centro de la circunferencia:");
					float x2 = float.Parse(Console.ReadLine());
					Console.WriteLine("Muy bien. Dame la y del centro de la circunferencia:");
					float y2 = float.Parse(Console.ReadLine());

					if (FenCircunferencia(x,y,r,x2,y2)){
						Console.WriteLine("Sí, efectivamente, está dentro de la circunferencia");
					} else {
						Console.WriteLine("Sentimos decepcionarle, pero no está dentro de la circunferencia.");
					}
					break;
				case 2:

					break;
				case 3:

					break;
				default:
					Console.WriteLine("Amos a ver qué quieres.");
					break;
			}

		}

		// Cálculo

		public static Boolean FenCircunferencia(float x, float y, float r,float x2,float y2) {
			if(Math.Round(Math.Pow((x - x2),2) +  Math.Pow((y - y2),2)) == Math.Round(Math.Pow(r,2))){
				return true;
			} else {
				return false;
			}
		}

	}
}
