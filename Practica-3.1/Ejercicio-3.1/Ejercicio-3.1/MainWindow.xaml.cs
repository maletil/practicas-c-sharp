﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ejercicio_3._1
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int num = int.Parse(entrada.Text);
            Generar(num);
        }
        private void Generar(int num)
        {
            int numOriginal = num;
            int dig = 0, c,primer = 0,ultimo = 0;
            for (c = 0; num != 0; c++)
            {
                dig = num % 10;
                num = num / 10; // Condición de paro
                if (c == 0){
                    primer = dig;
                }
            }
            ultimo = dig;
            MessageBox.Show("Primera: " + primer.ToString() + "\nÚltimo: " + ultimo.ToString());

            int primerComprobar=0, ultimoComprobar=0, generados=0;
            for (int i = numOriginal; generados != 10; i++)
            {
                num = i;
                for (c = 0; num != 0; c++)
                {
                    dig = num % 10;
                    num = num / 10; // Condición de paro
                    if (c == 0)
                    {
                        primerComprobar = dig;
                    }
                }
                ultimoComprobar = dig;
                MessageBox.Show("Primera: " + primerComprobar.ToString() + "\nÚltimo: " + ultimoComprobar.ToString());
                if (ultimo == ultimoComprobar && primer == primerComprobar)
                {
                    salida.Text += i+", ";
                    generados++;
                }
                MessageBox.Show(i.ToString(), "i");
            }
        }
    }
}
