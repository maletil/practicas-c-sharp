﻿//  Prácticas C Sharp: Ejercicio 37, REL.PROB 2 (Multiplicar y dividir)
//  Miguel Ángel Sánchez Herreros <MiguelAngel.Sanchez16@alu.uclm.es>
//
//  Copyright (c) 2018 Miguel Ángel Sánchez Herreros
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
using System;
namespace Ejercicio5 {
	public class MainClass {
		public static void Main() {
			int numero1, numero2;
			decimal producto, cociente, resto;

			numero1 = FpedirNumeros();
			numero2 = FpedirNumeros(2);
			FescribirSeparador();

			//Producto
			producto = Fproducto(numero1,numero2);
			Console.WriteLine("El producto es {0}",producto);

			//División
			if (Fdivision(numero1,numero2,out cociente,out resto)) {
				Console.WriteLine("El cociente de la división es {0} y el resto, {1}",cociente,resto);
			} else{
				Console.WriteLine("No se puede puede calcular la división entre un número mayor.");
			}
		}

		// Cálculo
		static decimal Fproducto(int num1, int num2){
			decimal producto = 0;
			if (num1 > 0) { // n>0
				for (int i = 0; i < num1; i++) {
					producto = producto + num2;
				}
			} else{ // n<0
				num1 = -num1;
				for (int i = 0; i < num1; i++) {
					producto = producto - num2;
				}
			}
			return producto;
		}
		static bool Fdivision(int num1,int num2, out decimal cociente, out decimal resto){
			if (num1 < num2) { cociente = 0;resto = 0; // n2>n1
				return false;
			}

			FdivisionPos(num1,num2,out cociente,out resto);

			if (Fproducto(num1,num2)<0) {  //Signos distintos
				cociente = -cociente;
			}
			return true;
		}
		static void FdivisionPos(int num1,int num2,out decimal cociente,out decimal sigNum){
			num1 = Math.Abs(num1);
			num2 = Math.Abs(num2);
			sigNum = num1;

			for (cociente = 0; sigNum >= num2; cociente++) {
				sigNum = sigNum - num2;
			}
		}


		// Interfaz
		static int FpedirNumeros(int num = 1){
			int respuesta;
			Console.WriteLine("Introduce el número {0}", num);
			respuesta = int.Parse(Console.ReadLine());
			while(respuesta == 0){
				Console.WriteLine("El número no puede ser 0, introduce el número {0}",num);
				respuesta = int.Parse(Console.ReadLine());
			}
			return respuesta;
		}
		static void FescribirSeparador(){
			Console.WriteLine("");
			Console.WriteLine("-----------------");
		}

	}
}
