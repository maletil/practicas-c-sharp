﻿//  Prácticas C Sharp: Ejercicio 28, REL.PROB 4 (Matrices numéricas)
//  Miguel Ángel Sánchez Herreros <MiguelAngel.Sanchez16@alu.uclm.es>
//
//  Copyright (c) 2018 Miguel Ángel Sánchez Herreros
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
using System;
using lib;
namespace Ejercicio19 {
	public class MainClass {
		public static void Main() {

			int[,] matriz1 = new int[4,4];
			int[,] matriz2 = new int[4,4];

			bool continuar = true;

			while (continuar) {
				Console.WriteLine("¿Qué quieres calcular?");
				Console.WriteLine("0. Salir del programa");
				Console.WriteLine("1. Introducir matriz");
				Console.WriteLine("2. Modificar posición de matriz");
				Console.WriteLine("3. Imprimir matriz");
				Console.WriteLine("4. Borrar matriz");
				Console.WriteLine("5. Sumar matrices");
				Console.WriteLine("6. Multiplicar matrices");
				Console.WriteLine("7. Trasponer matriz");
				int opcion = int.Parse(Console.ReadLine());

				while (opcion < 0 || opcion > 7) {
					Console.WriteLine("Opción incorrecta. Introduce una de las tres opciones.");
					opcion = int.Parse(Console.ReadLine());
				}

				if (opcion == 0) {
					continuar = false;
				} else {

					Console.WriteLine("¿Qué matriz quieres modificar? (1 o 2)");
					int n = int.Parse(Console.ReadLine());
					while (n != 1 && n != 2) {
						Console.WriteLine("Opción incorrecta. Introduce una de las dos matrices.");
						n = int.Parse(Console.ReadLine());
					}
					switch (opcion) {
						case 0:
							continuar = false;
							break;
						case 1: //Lectura
							if (n == 1) {
								Matriz.Lectura(matriz1);
							} else if (n == 2) {
								Matriz.Lectura(matriz2);
							}
							break;
						case 2: //Escribir
							Console.WriteLine("Posición a modificar: (fila,columna)");
							int fila, columna, valor;
							fila = int.Parse(Console.ReadLine()); 
							while (fila < 0 || fila > 3) {
								Console.WriteLine("Opción incorrecta. Introduce un número del 0 al 3.");
								fila = int.Parse(Console.ReadLine());
							}
							columna = int.Parse(Console.ReadLine());
							while (columna < 0 || columna > 3) {
								Console.WriteLine("Opción incorrecta. Introduce un número del 0 al 3.");
								columna = int.Parse(Console.ReadLine());
							}

							Console.WriteLine("Nuevo valor:");
							valor = int.Parse(Console.ReadLine());

							if (n == 1) {
								Matriz.Escribir(matriz1, fila, columna, valor);
							} else if (n == 2) {
								Matriz.Escribir(matriz2, fila, columna, valor);
							}
							break;
						case 3: //Imprimir
							if (n == 1) {
								Matriz.Imprimir(matriz1);
							} else if (n == 2) {
								Matriz.Imprimir(matriz2);
							}
							break;
						case 4: //Borrar
							if (n == 1) {
								Matriz.Borrar(matriz1);
							} else if (n == 2) {
								Matriz.Borrar(matriz2);
							}
							break;
						case 5: //Sumar
							if (n == 1) {
								Matriz.Suma(matriz1,matriz2);
							} else if (n == 2) {
								Matriz.Suma(matriz2,matriz1);
							}
							break;
						case 6: //Multiplicar
							if (n == 1) {
								Matriz.Multiplicar(matriz1,matriz2);
							} else if (n == 2) {
								Matriz.Multiplicar(matriz2,matriz1);
							}
							break;
						case 7: //Trasponer
							if (n == 1) {
								Matriz.Traspuesta(matriz1);
							} else if (n == 2) {
								Matriz.Traspuesta(matriz2);
							}
							break;
					}
				}
			}
		}
	}
}
