﻿using System;

namespace lib {
	public class Matriz {

		public static void Lectura(int[,] matriz) {
			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < 4; j++) {
					Console.WriteLine("Introduce el elemento {0},{1}",i,j);
					matriz[i,j] = int.Parse(Console.ReadLine());
				}
			}
		}

		public static void Escribir(int[,] matriz, int posf, int posc, int n) {
			matriz[posf,posc] = n;
		}

		public static void Imprimir(int[,] matriz) {
			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < 4; j++) {
					Console.Write("{0:D2} ",matriz[i,j]);
				}
				Console.WriteLine();
			}
			Console.WriteLine();
		}

		public static void Borrar(int[,] matriz) {
			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < 4; j++) {
					matriz[i,j] = 0;
				}
			}
		}

		public static void Suma(int[,] matriz1,int[,] matriz2) {
			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < 4; j++) {
					matriz1[i,j] += matriz2[i,j];
				}
			}
		}

		public static void Multiplicar(int[,] matriz1,int[,] matriz2) {
			int[,] resultado = new int[4,4];
			int sumas;

			for (int i = 0; i < 4; i++) { //FILAS DE 1
				for (int j = 0; j < 4; j++) { //COLUMNAS DE 2
					sumas = 0;
					for (int e = 0; e < 4; e++) { //CUATRO ELEMENTOS
						sumas += matriz1[i,e] * matriz2[e,j];
					}
					resultado[i,j] = sumas;
				}
			}

			for (int i = 0; i < 4; i++) { //Copiar
				for (int j = 0; j < 4; j++) {
					matriz1[i,j] = resultado[i,j];
				}
			}

		}

		public static void Traspuesta(int[,] matriz) {
			int[,] traspuesta = new int[4,4];

			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < 4; j++) {
					traspuesta[i,j] = matriz[j,i];
				}
			}
			for (int i = 0; i < 4; i++) { //Copiar
				for (int j = 0; j < 4; j++) {
					matriz[i,j] = traspuesta[i,j];
				}
			}
		}
	}
}
