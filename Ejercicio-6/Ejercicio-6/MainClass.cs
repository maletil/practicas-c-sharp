﻿//  Prácticas C Sharp: Ejercicio 35, REL.PROB 2 (Por métodos)
//  Miguel Ángel Sánchez Herreros <MiguelAngel.Sanchez16@alu.uclm.es>
//
//  Copyright (c) 2018 Miguel Ángel Sánchez Herreros
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
using System;
namespace Ejercicio6 {
	public class MainClass {
		static public void Main() {
			//Iniciar variables
			int grado1, grado2, grado3;

			//Pedir datos de entrada
			Console.WriteLine("Dame números en el formato ax²+bx+c");
			grado1 = FpedirNumeros('a');
			grado2 = FpedirNumeros('b');
			grado3 = FpedirNumeros('c');

			FescribirSeparador();

			FdentroRaiz(grado1,grado2,grado3);

			// Cálculo
			Console.WriteLine(resolverEcuacion(grado1,grado2,grado3));
		}

		// Cálculo
		static float FdentroRaiz(int grado1, int grado2, int grado3){
			return (float)(Math.Pow(grado2,2)) - (float)(4 * grado1 * grado3);
		}
		static String resolverEcuacion(int grado1,int grado2,int grado3){
			String Ssolucion;
			float dentroRaiz;

			dentroRaiz = FdentroRaiz(grado1,grado2,grado3);
			Console.WriteLine(dentroRaiz);
	        if (dentroRaiz >= 0) {
				double resultado1, resultado2;
				
				resultado1 = (-grado2 + Math.Sqrt(dentroRaiz)) / (2 * grado1);
				resultado2 = (-grado2 - Math.Sqrt(dentroRaiz)) / (2 * grado1);
				Ssolucion = "Los resultados son: " + resultado1 + " y " + resultado2;
			} else { //Números imaginarios
				float primeraparte, segundaparte;

				primeraparte = (float)-grado2 / (2 * grado1);
				segundaparte = (float)Math.Sqrt(-dentroRaiz) / (2 * grado1);
				Ssolucion = "Los resultados son: " + primeraparte + " ± " + segundaparte;
			}

			return Ssolucion;
		}

		// Interfaz
		static int FpedirNumeros(char num) {
			int respuesta;
			Console.WriteLine("Introduce el número {0}",num);
			respuesta = int.Parse(Console.ReadLine());
			while (respuesta == 0) {
				Console.WriteLine("El número no puede ser 0, introduce el número {0}",num);
				respuesta = int.Parse(Console.ReadLine());
			}
			return respuesta;
		}
		static void FescribirSeparador() {
			Console.WriteLine("-----------------");
			Console.WriteLine("");
		}

	}
}
