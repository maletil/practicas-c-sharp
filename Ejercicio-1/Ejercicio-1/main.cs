﻿//  Prácticas C Sharp: Ejercicio 30, REL.PROB 2 (Medias)
//  Miguel Ángel Sánchez Herreros <MiguelAngel.Sanchez16@alu.uclm.es>
//
//  Copyright (c) 2018 Miguel Ángel Sánchez Herreros
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
namespace Ejercicio1 {
	public class main {
		public static void Main (string [] args) {
			//Definición de variables globales
			int suma = 0, producto = 1;
			float suma_inversa = 0;
			double media_A, media_G, media_H;

			char respuestaBucle;
			int cuantosnum = 0;
			bool calcularMedia = false;

			do { //Bucle de calcularMedia
				suma = 0; producto = 1; suma_inversa = 0; // Reinicio de variables pafra futuros cálculos.
				//Menú
				int respuesta_menu;
				Console.WriteLine("¿Qué media quieres calcular?");
				Console.WriteLine("(0) Todas las medias");
				Console.WriteLine("(1) Media aritmética");
				Console.WriteLine("(2) Media geométrica");
				Console.WriteLine("(3) Media armónica");
				respuesta_menu = int.Parse(Console.ReadLine());

				while (respuesta_menu < 0 || respuesta_menu > 3) {
					Console.WriteLine("Opción incorrecta, por favor, vuelve a introducirla.");
					respuesta_menu = int.Parse(Console.ReadLine());
				}

				//Número de series
				Console.WriteLine("\n¿Cúantos números tiene tu serie?");
				cuantosnum = int.Parse(Console.ReadLine());
				//Error número < 1
				while (cuantosnum <= 1) {
					Console.WriteLine("Número incorrecto, por favor, introduce un número mayor que 1.");
					cuantosnum = int.Parse(Console.ReadLine());
				}

				//Cálculo de la media
				for (int contador = 1; contador <= cuantosnum; contador++) {
					int numero = 0;
					//Pedir números
					Console.WriteLine("Introduce el número {0}",contador);
					numero = int.Parse(Console.ReadLine());
					//Error número < 0
					while (numero < 0) {
						Console.WriteLine("Número incorrecto, por favor, introduce un número mayor que 0.");
						numero = int.Parse(Console.ReadLine());
					}
					//Cálculo series
					suma = suma + numero;
					producto = producto * numero;
					suma_inversa = suma_inversa + (float)1 / numero;
				}
				Console.WriteLine("\n----------------\n");
				//Cálculo de la respuesta
				switch (respuesta_menu) {
					case 0: //Todas las medias
						media_A = suma / cuantosnum;
						Console.WriteLine("La media aritmética es {0}",media_A);

						media_G = Math.Pow(producto,(double)1 / cuantosnum);
						Console.WriteLine("La media geométrica es {0}",media_G);

						media_H = (double)cuantosnum / suma_inversa;
						Console.WriteLine("La media armónica es {0}",media_H);
						break;
					case 1:
						media_A = suma / cuantosnum;
						Console.WriteLine("La media aritmética es {0}",media_A);
						break;
					case 2:
						media_G = Math.Pow(producto,(double)1 / cuantosnum);
						Console.WriteLine("La media geométrica es {0}",media_G);
						break;
					case 3:
						media_H = (double)cuantosnum / suma_inversa;
						Console.WriteLine("La media armónica es {0}",media_H);
						break;
					default:
						break;
				}
				// Seguir calculando medias
				Console.WriteLine("¿Quieres calcular otra media? (s/n)");
				respuestaBucle = char.Parse(Console.ReadLine());
				respuestaBucle = Char.ToLower(respuestaBucle);
				if (respuestaBucle == 's') {
					calcularMedia = true;
				} else { calcularMedia = false; }
			} while (calcularMedia);
		}	
	}
}
