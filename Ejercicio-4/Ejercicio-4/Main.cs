﻿//  Prácticas C Sharp: Ejercicio 36, REL.PROB 2 (Series de un número dado)
//  Miguel Ángel Sánchez Herreros <MiguelAngel.Sanchez16@alu.uclm.es>
//
//  Copyright (c) 2018 Miguel Ángel Sánchez Herreros
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.


using System;
namespace Ejercicio4 {
	class MainClass {
		public static void Main(string [] args) {
			int numero = 0, simple, cuadrado, cubo;
			int numero_series = 10;

			Console.WriteLine("Introduce un número");
			numero = int.Parse(Console.ReadLine());
			Console.WriteLine("");

			Console.Write("Número\t\t");
			Console.Write("Cuadrado\t   ");
			Console.WriteLine("Cubo");
			for (int i = 1; i <= numero_series; i++){
				simple = numero + i;
				cuadrado = (int)Math.Pow(simple,2);
				cubo = (int)Math.Pow(simple,3);

				Console.WriteLine("{0}\t\t{1}\t\t   {2}",simple,cuadrado,cubo);
			}

		}
	}
}
