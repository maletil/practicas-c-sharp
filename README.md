# Prácticas C# #

Prácticas de programación para la asignatura de Informática

## Listado de ejercicios:
### Ejercicio-1

Ejercicio 30, REL.PROB 2 (Medias)

### Ejercicio-2

Ejercicio 32, REL.PROB 2 (Sucesión Fibonacci)

### Ejercicio-3

Ejercicio 35, REL.PROB 2 (Cálculo de ecuaciones de segundo grado)

### Ejercicio-4

Ejercicio 36, REL.PROB 2 (Series de un número dado)

### Ejercicio-5

Ejercicio 37, REL.PROB 2 (Multiplicar y dividir)

### Ejercicio-6

Ejercicio 35, REL.PROB 2 (Por métodos)

### Ejercicio-6.2

Ejercicio 35, REL.PROB 2 (Con librería)

### Ejercicio-7

Ejercicio 42, REL.PROB 2 (Sistema dos ecuaciones dos incógnitas)

### Ejercicio-8

Ejercicio 23, REL.PROB 2 (Polinomio de Taylor del cos)

### Ejercicio-9

Ejercicio 12, REL.PROB 3 (Coordenadas de un punto)

### Ejercicio-10

Ejercicio 16, REL.PROB 3 (Horas del día)

### Ejercicio-11

Ejercicio 17, REL.PROB 3 (Triángulos)

### Ejercicio-12

Ejercicio 12, REL.PROB 3 (con lib).

### Ejercicio-13

Ejercicio 15, REL.PROB 4 (Caracter en blanco)

### Ejercicio-14

Ejercicio 24, REL.PROB 4 (Frecuencia de notas)

### Ejercicio-15

Ejercicio 32, REL.PROB 4 (Operaciones con matrices)

### Ejercicio-16

Ejercicio 33, REL.PROB 4 (Modificar matriz)

### Ejercicio-17

Ejercicio 25, REL.PROB 4 (Cifrado César)

### Ejercicio-18

Ejercicio 29, REL.PROB 4 (Manipulación de matrices)

### Ejercicio-19

Ejercicio 28, REL.PROB 4 (Matrices numéricas)

## Nota: 
 Las librerías de cada ejercicio se encuentran en las soluciones llamadas 'lib' situdadas en cada directorio.
