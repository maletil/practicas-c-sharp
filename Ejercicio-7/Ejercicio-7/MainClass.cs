﻿//  Prácticas C Sharp: Ejercicio 42, REL.PROB 2 (Sistema dos ecuaciones dos incógnitas)
//  Miguel Ángel Sánchez Herreros <MiguelAngel.Sanchez16@alu.uclm.es>
//
//  Copyright (c) 2018 Miguel Ángel Sánchez Herreros
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
using System;
namespace Ejercicio7 {
	public class MainClass {
		static public void Main() {
			float a, b, c, d, e, f;
			double x = 0 , y = 0;
			// Formato: ax+by=c; dx+ey=f
			Console.WriteLine("Dame números de la primera ec. en el formato: ax + by = c");

			Console.WriteLine("Introduce el número a");
				a = int.Parse(Console.ReadLine());
			Console.WriteLine("Introduce el número b");
				b = int.Parse(Console.ReadLine());
			Console.WriteLine("Introduce el número c");
				c = int.Parse(Console.ReadLine());

			Console.WriteLine("\nDame números de la segunda ec. en el formato: dx + ey = f");

			Console.WriteLine("Introduce el número d");
				d = int.Parse(Console.ReadLine());
			Console.WriteLine("Introduce el número e");
				e = int.Parse(Console.ReadLine());
			Console.WriteLine("Introduce el número f");
				f = int.Parse(Console.ReadLine());

			Console.WriteLine("-----------------");
			Console.WriteLine("");

			Console.WriteLine("El sistema es: ");
			Console.WriteLine("{0}x + {1}y = {2}",a,b,c);
			Console.WriteLine("{0}x + {1}y = {2}",d,e,f);

			switch (FcalculoIncognitas(a,b,c,d,e,f,ref x,ref y)) {
				case 0:
					Console.WriteLine("Es un sistema compatible indeterminado, tiene infinitas soluciones.");
					break;
				case 1:
					Console.WriteLine("Es un sistema incompatible, no tiene solución.");
					break;
				case 2:
					Console.WriteLine("La x es {0}, y la y es {1}",x,y);
					break;
			}
		}

		// Cálculo

		static int FcalculoIncognitas(float a,float b,float c,float d,float e,float f,ref double x,ref double y) {
			float p1 = (f * a - d * c), p2 = (a * e - d * b);
			if (p1 == 0 && p2 == 0) {
				return 0;
			} else if (p2 == 0) {
				return 1;
			} else {
				y = (double)p1 / p2;
				x = (double)(c - b * y) / a;
				return 2;
			}
		}

	}
}
