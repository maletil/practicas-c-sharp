﻿using System;

namespace lib {
	public class Reloj {
		public static void DevolverHora(out int hora, out int minutos, out int segundos){
			hora = int.Parse(Console.ReadLine());
			minutos = int.Parse(Console.ReadLine());
			segundos = int.Parse(Console.ReadLine());
		}
		public static void MostrarHora(int hora, int minutos, int segundos) {
			Console.WriteLine("La hora es {0}:{1}:{2}",hora,minutos,segundos);
		}
		public static int DevolverHorasDelDia(int hora,int minutos,int segundos, out int horaS) {
			horaS = hora * 3600 + minutos * 60 + segundos;

			if (hora > 6 && hora < 15) {
				return 1;
			} else if (hora >= 15 && hora < 20) {
				return 2;
			} else {
				return 3;
			}
		}
	}
}
