﻿//  Prácticas C Sharp: Ejercicio 16, REL.PROB 3 (Horas del día)
//  Miguel Ángel Sánchez Herreros <MiguelAngel.Sanchez16@alu.uclm.es>
//
//  Copyright (c) 2018 Miguel Ángel Sánchez Herreros
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
using System;
using System.Threading;
using lib;

namespace Ejercicio10 {
	public class MainClass {
		public static void Main() {
			do {
				int hora1, minutos1, segundos1, hora2, minutos2, segundos2;

				Console.WriteLine("Dame la primera hora");
				Reloj.DevolverHora(out hora1,out minutos1,out segundos1);
				Console.WriteLine("Dame la segunda hora");
				Reloj.DevolverHora(out hora2,out minutos2,out segundos2);
				Console.WriteLine();
				Thread.Sleep(1000);

				Reloj.MostrarHora(hora1,minutos1,segundos1);
				Reloj.MostrarHora(hora2,minutos2,segundos2);
				Console.WriteLine();
				Thread.Sleep(1000);

				int hora1S, hora2S;
				Console.Write("La primera hora ");
				switch (Reloj.DevolverHorasDelDia(hora1,minutos1,segundos1,out hora1S)) {
					case 1:
						Console.WriteLine("es por la mañana.");
						break;
					case 2:
						Console.WriteLine("es por la tarde.");
						break;
					case 3:
						Console.WriteLine("es por la noche.");
						break;
				}
				Console.WriteLine("El equivalente en segundos de la primera hora es: {0}",hora1S);
				Console.WriteLine();
				Console.Write("La segunda hora ");
				switch (Reloj.DevolverHorasDelDia(hora2,minutos2,segundos2,out hora2S)) {
					case 1:
						Console.WriteLine("es por la mañana.");
						break;
					case 2:
						Console.WriteLine("es por la tarde.");
						break;
					case 3:
						Console.WriteLine("es por la noche.");
						break;
				}
				Console.WriteLine("El equivalente en segundos de la segunda hora es: {0}",hora2S);
				Console.WriteLine();
				Thread.Sleep(500);
				Console.WriteLine("¿Quieres seguir usando el reloj? (s/n)");
				char r = char.Parse(Console.ReadLine());
				if (char.ToLower(r) != 's') {
					break;
				}
			} while (true);
		}
	}
}
