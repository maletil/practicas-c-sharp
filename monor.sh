#! /bin/bash
#Ejecuta el ejercicio pasado por $1 con mono

if [[ !"$PWD" =~ bin ]]; then
  clear && printf '\n\n' && mono *.exe;
elif [[ -n $1 ]];then
  cd $1"/"$1"/bin/Debug"; 
  clear && printf '\n\n' && mono *.exe;
else
  echo "Por favor, introduzca el nombre del directorio";
fi

