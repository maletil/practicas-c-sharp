﻿//  Prácticas C Sharp: Práctica 4 (Números primos)
//  Miguel Ángel Sánchez Herreros <MiguelAngel.Sanchez16@alu.uclm.es>
//
//  Copyright (c) 2018 Miguel Ángel Sánchez Herreros
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_4
{
    class Program
    {
        static void Main(string[] args)
        {
            int numeroMayores;
            ImprimePrimos(90,9, out numeroMayores);
            Console.WriteLine("{0} primos son mayores que 100",numeroMayores);
        }
        public static void ImprimePrimos(int inicio, int num,out int NumeroMayores){
            NumeroMayores = 0; // Número de primos mayores que 100
            int generados = 0;
            for (int j = inicio; generados < num; j++) {
                
                if (Primo(j)) {
                    Console.WriteLine(j.ToString());
                    generados++;
                    if (j > 100) { NumeroMayores++; }
                }
            }
        }

        public static bool Primo(int num) {
            if (num == 1) { return true; }
            for(int i = 2; i < num; i++) {
                if(num % i == 0) { //DIVISIBLE
                    return false;
                }
            }
            return true;
        }
    }
}
